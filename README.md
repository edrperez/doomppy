# Doomppy Launcher #

Lanzador gráfico para Doom escrito en Python usando Tkinter, funciona en Linux. Aunque con modificaciones, por ejemplo remover lo de la búsqueda de source ports, podría funcionar en Windows sin problemas.

El código está dividido en 3 partes principales:

* Una clase que escribió otra persona que permite manipular elementos en un listbox (cambiar el orden), le agregué la opción para eliminar elementos al hacer doble click.
* Las funciones que interactuan con la interfaz gráfica.
* El código que crea la interfaz gráfica en si.

Más información en mi [sitio Web](https://www.edrperez.com/hora-del-codigo/hora-del-codigo-12/).