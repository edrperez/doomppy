#! /usr/bin/python3
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import tkinter as tk
from tkinter.filedialog import askopenfilename, askopenfilenames, asksaveasfilename
import os
import json
import subprocess
import webbrowser
from functools import partial

#Esta clase no me pertenece
class ReorderableListbox(tk.Listbox):
    """ A Tkinter listbox with drag & drop reordering of lines """
    #Original de https://stackoverflow.com/a/53823708
    #Agregué el evento doble click para eliminar lo seleccionado,
    #así como el método Delete
    def __init__(self, master, **kw):
        kw['selectmode'] = tk.EXTENDED
        tk.Listbox.__init__(self, master, kw)
        self.bind('<Button-1>', self.setCurrent)
        self.bind('<Double-Button-1>', self.Delete)
        self.bind('<Control-1>', self.toggleSelection)
        self.bind('<B1-Motion>', self.shiftSelection)
        self.bind('<Leave>',  self.onLeave)
        self.bind('<Enter>',  self.onEnter)
        self.selectionClicked = False
        self.left = False
        self.unlockShifting()
        self.ctrlClicked = False
    def orderChangedEventHandler(self):
        pass

    def Delete(self, event):
        selection = self.curselection()
        for i in reversed(selection):
            self.delete(i)

    def onLeave(self, event):
        # prevents changing selection when dragging
        # already selected items beyond the edge of the listbox
        if self.selectionClicked:
            self.left = True
            return 'break'
    def onEnter(self, event):
        #TODO
        self.left = False

    def setCurrent(self, event):
        self.ctrlClicked = False
        i = self.nearest(event.y)
        self.selectionClicked = self.selection_includes(i)
        if (self.selectionClicked):
            return 'break'

    def toggleSelection(self, event):
        self.ctrlClicked = True

    def moveElement(self, source, target):
        if not self.ctrlClicked:
            element = self.get(source)
            self.delete(source)
            self.insert(target, element)

    def unlockShifting(self):
        self.shifting = False
    def lockShifting(self):
        # prevent moving processes from disturbing each other
        # and prevent scrolling too fast
        # when dragged to the top/bottom of visible area
        self.shifting = True

    def shiftSelection(self, event):
        if self.ctrlClicked:
            return
        selection = self.curselection()
        if not self.selectionClicked or len(selection) == 0:
            return

        selectionRange = range(min(selection), max(selection))
        currentIndex = self.nearest(event.y)

        if self.shifting:
            return 'break'

        lineHeight = 15
        bottomY = self.winfo_height()
        if event.y >= bottomY - lineHeight:
            self.lockShifting()
            self.see(self.nearest(bottomY - lineHeight) + 1)
            self.master.after(500, self.unlockShifting)
        if event.y <= lineHeight:
            self.lockShifting()
            self.see(self.nearest(lineHeight) - 1)
            self.master.after(500, self.unlockShifting)

        if currentIndex < min(selection):
            self.lockShifting()
            notInSelectionIndex = 0
            for i in selectionRange[::-1]:
                if not self.selection_includes(i):
                    self.moveElement(i, max(selection)-notInSelectionIndex)
                    notInSelectionIndex += 1
            currentIndex = min(selection)-1
            self.moveElement(currentIndex, currentIndex + len(selection))
            self.orderChangedEventHandler()
        elif currentIndex > max(selection):
            self.lockShifting()
            notInSelectionIndex = 0
            for i in selectionRange:
                if not self.selection_includes(i):
                    self.moveElement(i, min(selection)+notInSelectionIndex)
                    notInSelectionIndex += 1
            currentIndex = max(selection)+1
            self.moveElement(currentIndex, currentIndex - len(selection))
            self.orderChangedEventHandler()
        self.unlockShifting()
        return 'break'

class GUI(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.title("Doomppy Launcher 1.1")
        self.contenedor = tk.Frame(self)
        self.contenedor.grid(row=0, column=1, sticky="news")
        self.contenedor.grid_rowconfigure(0, weight=1)
        self.contenedor.grid_columnconfigure(1, weight=1)

        #Asignación inicial de directorios y nombre del archivo temporal
        self.configuracion_temporal = 'doomppy.tmp'
        self.configuracion_actual = ""
        self.iwad_dir = "/root/doom"
        self.pwad_dir = "/root/doom"
        self.source_port_dir = "/root/doom"
        self.idiomas_archivo = "idiomas.json"
        self.idioma_elegido = "esp"
        self.idiomas_disponibles = ""
        self.idiomas_cadenas = ""
        self.idioma_almacenado = True

        #Los source ports a buscar usando which en linux
        self.source_ports = ['zdoom', 'lzdoom', 'gzdoom', 'crispy-doom', 'chocolate-doom', 'prboom-plus']

        #La imagen del botón de jugar
        logo_doom = tk.PhotoImage(file = r"logo_doom.png") 
        self.logo_jugar = logo_doom.subsample(4, 4) 

        self.cargar_idiomas()
        self.frames = {}
        self.crear_frames(self.frames)
        #Pega la tecla F5 a la función jugar
        self.bind_all('<F5>', lambda e: self.frames["FrameDerecho"].jugar())
        #Al iniciar el programa buscamos el archivo de configuración temporal y si lo encuentra cargamos los datos
        self.recargar_configuracion_actual()
        
    def recargar_configuracion_actual(self):
        """Lee la configuración en el inicio y en el cambio de idioma"""
        if not self.configuracion_actual:
            configuracion = self.configuracion_temporal
        else:
            configuracion = self.configuracion_actual
        if os.path.isfile(configuracion) and os.stat(configuracion).st_size > 0:
                self.leer_configuracion(configuracion)

    def crear_frames(self, frames):
        """Crea los frames"""
        self.frames["FrameIzquierdo"] = FrameIzquierdo(padre=self.contenedor, controlador=self) 
        self.frames["FrameDerecho"] = FrameDerecho(padre=self.contenedor, controlador=self)
        self.frames["FrameIzquierdo"].grid(row=0, column=0, sticky="nsew")
        self.frames["FrameDerecho"].grid(row=0, column=1, sticky="nsew")

    def recrear_frames(self):
        """Destruye los frames y solicita la creación"""
        self.frames["FrameDerecho"].destroy()
        self.frames["FrameIzquierdo"].destroy()
        self.crear_frames(self.frames)

    def seleccionar_idioma(self, idioma):
        """Define el idioma elegido"""
        self.idioma_elegido = idioma
        self.idioma_almacenado = False
        self.recargar_configuracion_actual()

    def generar_botones_idiomas(self, fr_botones_idiomas):
        """Genera botones para cambiar de idioma"""
        i = 0
        for idi in self.idiomas_disponibles:
            tk.Button(fr_botones_idiomas, text=idi, command=partial(self.seleccionar_idioma, idi)).grid(row=i, column=0, sticky="ew", padx=5, pady=5)
            i += 1

    def cargar_idiomas(self):
        """Cargar los idiomas disponibles y su contenido"""
        with open(self.idiomas_archivo) as json_file:        
            data = json.load(json_file)
            self.idiomas_disponibles = data.keys()
            self.idiomas_cadenas = data

    def guardar_configuracion(self):
        """Pregunta el nombre del archivo para guardar la configuración y luego llama a la función que lo escribe"""
        archivo = asksaveasfilename()

        if not archivo:
            return

        self.escribir_configuracion(archivo)
        
    def escribir_configuracion(self, archivo):
        """Escribe la configuración en el archivo designado"""
        iwad = self.frames["FrameDerecho"].txt_iwad.get()
        pwad = ' '.join(self.splitlist(self.frames["FrameDerecho"].listbox_pwads.get(0, tk.END)))
        source_port = self.frames["FrameDerecho"].txt_source_port.get()
        
        if not iwad or not source_port:
            return

        data = {}
        data['iwad'] = iwad
        data['iwad_dir'] = os.path.dirname(iwad)
        data['source_port'] = source_port
        data['source_port_dir'] = os.path.dirname(source_port)
        data['idioma'] = self.idioma_elegido
        
        if not pwad:
            data['pwad'] = ""
            data['pwad_dir'] = ""
        else:
            data['pwad'] = self.frames["FrameDerecho"].listbox_pwads.get(0, tk.END)
            data['pwad_dir'] = os.path.dirname(data['pwad'][0])
        with open(archivo, 'w') as outfile:
            json.dump(data, outfile, indent=4)

    def leer_configuracion(self, archivo):
        """Lee la configuración en el archivo designado y solicita la recreación de los frames"""
        with open(archivo) as json_file:        
            data = json.load(json_file)

            self.iwad_dir = data['iwad_dir']
            self.pwad_dir = data['pwad_dir']
            self.source_port_dir = data['source_port_dir']
            if self.idioma_almacenado == True:
                self.idioma_elegido = data['idioma']
            self.recrear_frames()

            self.frames["FrameDerecho"].txt_source_port.delete(0, tk.END)
            self.frames["FrameDerecho"].txt_source_port.insert(tk.END, data['source_port'])

            self.frames["FrameDerecho"].txt_iwad.delete(0, tk.END)
            self.frames["FrameDerecho"].txt_iwad.insert(tk.END, data['iwad'])

            self.frames["FrameDerecho"].listbox_pwads.delete(0, tk.END)
            
            for p in self.splitlist(data['pwad']):
                self.frames["FrameDerecho"].listbox_pwads.insert(tk.END, p)

    def cargar_configuracion(self):
        """Carga una configuración guardada anteriormente"""

        archivo = askopenfilename()
        if not archivo:
            return
        
        self.idioma_almacenado = True
        self.configuracion_actual = archivo
        self.leer_configuracion(archivo)

class FrameDerecho(tk.Frame):

    def __init__(self, padre, controlador):
        tk.Frame.__init__(self, padre)
        self.controlador = controlador

        #Los botones del frame derecho
        btn_iwad = tk.Button(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Buscar iWAD"], command=self.seleccionar_iwad)
        btn_pwad = tk.Button(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Buscar pWADs y otros"], command=self.seleccionar_pwad)
        btn_guardar_configuracion = tk.Button(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Guardar configuración"], command=self.controlador.guardar_configuracion)
        btn_cargar_configuracion = tk.Button(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Cargar configuración"], command=lambda: self.controlador.cargar_configuracion())
        btn_ventana_about = tk.Button(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Acerca de"], command=self.ventana_about)
        btn_cerrar_doomppy = tk.Button(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Salir"], command=self.cerrar_doomppy)
        btn_jugar = tk.Button(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Jugar"], command=self.jugar, image=self.controlador.logo_jugar, compound = "bottom")
        #Las cajas de texto
        self.txt_source_port = tk.Entry(self)
        self.txt_iwad = tk.Entry(self)
        #Las etiquetas
        label_source_port = tk.Label(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["El source port"])
        label_iwad = tk.Label(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["El iWAD"])
        label_pwad = tk.Label(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Los pWADS y otros"])
        label_pwad_explicacion = tk.Label(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Instrucciones listbox"], anchor="w", justify="left")
        #Creamos el listbox re ordenable para recibir y manipular los pwads y otros archivos
        self.listbox_pwads = ReorderableListbox(self)
        #Colocamos todo en el frame
        btn_iwad.grid(row=3, column=0, sticky="ew", padx=5, pady=5)
        btn_pwad.grid(row=6, column=0, sticky="ew", padx=5, pady=5)
        btn_jugar.grid(row=9, column=0, sticky="ew", padx=5, pady=5)
        btn_guardar_configuracion.grid(row=10, column=0, sticky="ew", padx=5)
        btn_cargar_configuracion.grid(row=11, column=0, sticky="ew", padx=5)
        btn_ventana_about.grid(row=12, column=0, sticky="ew", padx=5)
        btn_cerrar_doomppy.grid(row=13, column=0, sticky="ew", padx=5)
        label_source_port.grid(row=0, column=0, sticky="ew", padx=5, pady=5)
        label_iwad.grid(row=2, column=0, sticky="ew", padx=5, pady=5)
        label_pwad.grid(row=5, column=0, sticky="news", padx=5, pady=5)
        label_pwad_explicacion.grid(row=7, column=0, sticky="news", padx=5, pady=5)
        self.txt_source_port.grid(row=1, column=0, sticky="new")
        self.txt_iwad.grid(row=4, column=0, sticky="new")
        self.listbox_pwads.grid(row=8, column=0, sticky="new")

    def seleccionar_iwad(self):
        """Selecciona un IWAD."""
        wads_extensiones = ['*.wad', '*.waD', '*.wAD',  '*.Wad', '*.WAd', '*.wAd', '*.WAD']
        archivo = askopenfilename(initialdir = self.controlador.iwad_dir,
            filetypes=[("WADS", wads_extensiones), (self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Todos"], "*.*")]
        )
        if not archivo:
            return

        self.txt_iwad.delete(0, tk.END)
        self.txt_iwad.insert(tk.END, archivo)

    def seleccionar_pwad(self):
        """Selecciona pWAD."""
        wads_extensiones = ['*.wad', '*.waD', '*.wAD',  '*.Wad', '*.WAd', '*.wAd', '*.WAD']
        archivos = askopenfilenames(initialdir = self.controlador.pwad_dir,
            filetypes=[("WADS", wads_extensiones), (self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Todos"], "*.*")]
        )
        if not archivos:
            return
        for p in self.controlador.tk.splitlist(archivos):
            self.listbox_pwads.insert(tk.END, p)

    def ventana_about(self):
        """Publicidad a mi sitio"""
        ventana_about = tk.Toplevel(self.controlador)
        ventana_about.title(self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Acerca de"])
        ventana_about.rowconfigure(0, weight=1)
        ventana_about.columnconfigure(1, weight=1)
        ventana_about.resizable(0,0)
        
        def visitar_sitio():
            webbrowser.open_new('edrperez.com')
            ventana_about.destroy()
            
        tk.Label(ventana_about, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Acerca de texto 1"]).grid(row=0, column=0, sticky="ew", padx=5, pady=5)
        tk.Button(ventana_about, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Acerca de texto 2"], command=visitar_sitio).grid(row=1, column=0, sticky="ew", padx=5, pady=5)
        ventana_about.bind('<Return>', lambda e: visitar_sitio())
        ventana_about.bind('<KP_Enter>', lambda e: visitar_sitio())
        
        pos_x = int((self.controlador.winfo_x() + self.controlador.winfo_width() / 2) - ventana_about.winfo_reqwidth())
        pos_y = int((self.controlador.winfo_y() + self.controlador.winfo_height() / 2) - ventana_about.winfo_reqheight())
        ventana_about.geometry("+{}+{}".format(pos_x, pos_y))
        ventana_about.grab_set()
        
    def cerrar_doomppy(self):
        """"¡Destruyelo!"""
        self.controlador.destroy()

    def jugar(self):
        """A jugar."""
        iwad = self.txt_iwad.get()
        pwad = ' '.join(self.controlador.splitlist(self.listbox_pwads.get(0, tk.END)))
        source_port = self.txt_source_port.get()
        
        if not iwad or not source_port:
            return

        self.controlador.escribir_configuracion(self.controlador.configuracion_temporal)

        if pwad:
             pwad_par = " -file " + pwad
        else:
             pwad_par = ""

        comando = "{0} -iwad {1}{2}".format(source_port, iwad, pwad_par)
        subprocess.Popen(comando, shell=True)

class FrameIzquierdo(tk.Frame):

    def __init__(self, padre, controlador):
        tk.Frame.__init__(self, padre)
        self.controlador = controlador
        #Variable para recibir el source port seleccionado con los botones de radio
        self.source_port_seleccionado = tk.StringVar()
        self.source_port_seleccionado.set("")
        #Frame izquierdo que desplega los source ports encontrados y da opción para buscar uno a mano
        #Incluye la selección de idiomas
        #Los botones
        label_idioma = tk.Label(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Idioma"])
        label_idioma.grid(row=0, column=0, sticky="ew", padx=5, pady=5)
        fr_botones_idiomas  = tk.Frame(self, relief=tk.RAISED, bd=2)
        fr_botones_idiomas.grid_columnconfigure(0, weight=1)
        fr_botones_idiomas.grid(row=1, column=0, sticky="news")
        btn_source_port = tk.Button(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Seleccionar source port 1"], command=self.seleccionar_source_port)
        btn_source_port.grid(row=2, column=0, sticky="ew", padx=5, pady=5)
        label_source_ports_encontrados = tk.Label(self, text=self.controlador.idiomas_cadenas[self.controlador.idioma_elegido]["Seleccionar source port 2"])
        label_source_ports_encontrados.grid(row=3, column=0, sticky="ew", padx=5, pady=5)
        #En la carga del programa buscamos los source ports en el sistema y creamos las opciones de radio
        self.controlador.generar_botones_idiomas(fr_botones_idiomas)
        self.buscar_source_ports()
        self.grid_columnconfigure(0, weight=1)

    def elegir_source_port(self):
        """Elige el source port de las opciones de radio"""
        self.controlador.frames["FrameDerecho"].txt_source_port.delete(0, tk.END)
        self.controlador.frames["FrameDerecho"].txt_source_port.insert(tk.END, self.source_port_seleccionado.get())

    def buscar_source_ports(self):
        """Busca los source ports comunes"""
        
        i = 4
        for s in self.controlador.source_ports:
            out = subprocess.Popen(['which', s], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            stdout,stderr = out.communicate()
            
            tk.Radiobutton(self, 
                  text=stdout.decode('utf-8').rstrip(),
                  padx = 1, 
                  variable = self.source_port_seleccionado,
                  command=self.elegir_source_port,
                  value=stdout.decode('utf-8').rstrip()).grid(row=i, column=0, sticky="ew", padx=5, pady=5)
            i += 1

    def seleccionar_source_port(self):
        """Selecciona un source port."""
        archivo = askopenfilename(initialdir = self.controlador.source_port_dir)
        if not archivo:
            return

        self.controlador.frames["FrameDerecho"].txt_source_port.delete(0, tk.END)
        self.controlador.frames["FrameDerecho"].txt_source_port.insert(tk.END, archivo)

gui = GUI()
gui.mainloop()